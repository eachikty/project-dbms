@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            <div>
              <a href="{{ route('account.create') }}" class="btn btn-success">เปิดบัญชี</a>
            </div>
                <div class="panel-heading">
                <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">ACC_NO</th>
      <th scope="col">ACC_NAME</th>
      <th scope="col">ACC_Surname</th>
      <th scope="col">balance</th>
      <th scope="col" colspan=2>Operation</th>
    </tr>
  </thead>
  <?php $i=1; ?>
  @foreach($account as $row)
  <tbody>
    <tr>
      <th scope="row"><?php echo $i++; ?></th>
      <td>{{ $row->ACC_NO }}</td>
      <td>{{ $row->ACC_NAME }}</td>
      <td>{{ $row->ACC_Surname }}</td>
      <td>{{ $row->Balance }}</td>
      <td><a href="{{ route('account.edit',$row->id) }}" class="btn btn-warning">Edit</a></td>
      <td><form action="{{ route('account.destroy',$row->id) }}" method="post">
      @csrf
      @method("DELETE")
      <button class="btn btn-danger">Delete</button></form></td>
    </tr>
  </tbody>
  @endforeach
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
