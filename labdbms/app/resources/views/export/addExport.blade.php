<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 700px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <ul class="nav nav-pills nav-stacked">
        <li><a href="/">หน้าหลัก</a></li>
        <li><a href="/product">สินค้า</a></li>
        <li class="active"><a href="/export">เบิกสินค้า</a></li>
        <li><a href="/borrow">ยืมสินค้า</a></li>
        <li><a href="/employee">พนักงาน</a></li>
      </ul><br>
    </div>
    
    <div class="col-sm-10 text-left"> 

      <div class="row">
        <div class="col-sm-12 text-center">
          <div class="well" style="background-color:rgb(255, 212, 71)">
          <h4>ใบเบิกสินค้า</h4>
          </div>
        </div>
      </div>
      <!-- <h2 text-center>ใบเบิกสินค้า</h2> -->




      <div class="well">
        <table class="table table-condensed">
          <form method="post" action="{{ route('export.store') }}">
              {{ csrf_field() }}
                <div class="form-group">
                  <label class="control-label col-sm-2" for="exp_id">เลขที่ใบเบิก:</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control{{ $errors->has('exp_id') ? ' is-invalid' : '' }}"  id="exp_id" placeholder="ACC001" name="exp_id" maxlength="5" >
                      @if($errors->has('exp_id'))
                      <div class="alert alert-danger">
                        <strong>{{ $errors->first('exp_id') }}</strong>
                      </div>
                      @endif
                  </div>
                </div>
                <br>
                <br>

                <div class="form-group">
                  <label class="control-label col-sm-2" for="approve_emp_id">ผู้อนุมัติ:</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control{{ $errors->has('approve_emp_id') ? ' is-invalid' : '' }}"  id="approve_emp_id" placeholder="m0001" name="approve_emp_id" maxlength="5">
                      @if($errors->has('approve_emp_id'))
                      <div class="alert alert-danger">
                        <strong>{{ $errors->first('approve_emp_id') }}</strong>
                      </div>
                      @endif
                  </div>
                </div>
                <br>
                <br>

                <br>
                <br>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="prod_id">รหัสของสินค้า:</label>
                  <div class="col-sm-4">
                      <input type="text" class="form-control{{ $errors->has('prod_id') ? ' is-invalid' : '' }}"  id="prod_id" placeholder="P0001" name="prod_id" maxlength="5">
                      @if($errors->has('prod_id'))
                      <div class="alert alert-danger">
                        <strong>{{ $errors->first('prod_id') }}</strong>
                      </div>
                      @endif
                  </div>


                  <label class="control-label col-sm-1" for="exp_qty">จำนวน:</label>
                  <div class="col-sm-4">
                      <input type="number" class="form-control{{ $errors->has('exp_qty') ? ' is-invalid' : '' }}"  id="exp_qty" placeholder="5" name="exp_qty">
                      @if($errors->has('exp_qty'))
                      <div class="alert alert-danger">
                        <strong>{{ $errors->first('exp_qty') }}</strong>
                      </div>
                      @endif
                  </div>
                  <label class="control-label col-sm-1" >ชิ้น</label>

                </div>  

                <br>
                <br>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="request_emp_id">ผู้เบิก:</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control{{ $errors->has('request_emp_id') ? ' is-invalid' : '' }}"  id="request_emp_id" placeholder="m0002" name="request_emp_id" maxlength="5">
                      @if($errors->has('request_emp_id'))
                      <div class="alert alert-danger">
                        <strong>{{ $errors->first('request_emp_id') }}</strong>
                      </div>
                      @endif
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group" text-center>
                <label class="col-sm-4"> </label>
                  <button type="submit" class="btn btn-primary col-sm-4 ">บันทึก</button>   
                <label class="col-sm-4"> </label>
        
                </div>

          </form> 
        </table>
      </div>
    </div>
  </div>
</div>
<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>


</body>
</html>


