<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'Account';

    protected $fillable = ['ACC_NO','ACC_NAME','ACC_Surname','Balance'];
}
