<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export extends Model
{
    protected $table = 'Exports';
    protected $fillable = ['exp_id','approve_emp_id','exp_date','exp_id','prod_id','exp_qty','request_emp_id'];

}
