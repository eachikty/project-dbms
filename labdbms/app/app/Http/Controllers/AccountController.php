<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = Account::all();
        return view('account.index',compact('account'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'ACC_NO' => 'required|max:10',
                'ACC_NAME' => 'required|max:50',
                'ACC_Surname' => 'required|max:100',
                'Balance' => 'required'
            ]
        );
        $account = new Account;
        $account->ACC_No = $request->get('ACC_NO');
        $account->ACC_Name = $request->get('ACC_NAME');
        $account->ACC_Surname = $request->get('ACC_Surname');
        $account->Balance = $request->get('Balance');
        $account->save();

        return redirect('account');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::find($id);
        return view('account.edit',compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account = Account::find($id);

        $request->validate(
            [
                'ACC_NO' => 'required|max:10',
                'ACC_NAME' => 'required|max:50',
                'ACC_Surname' => 'required|max:100',
                'Balance' => 'required'
            ]
        );

        $account->ACC_NO = $request->ACC_NO;
        $account->ACC_NAME = $request->ACC_NAME;
        $account->ACC_Surname = $request->ACC_Surname;
        $account->Balance = $request->Balance;

        $account->update();
        return redirect("account");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::find($id);
        $account->delete();
        return redirect("account");
    }
}
