<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'Products';

    protected $fillable = ['prod_id','cat_id','prod_name','prod_qty','prod_price'];
}
