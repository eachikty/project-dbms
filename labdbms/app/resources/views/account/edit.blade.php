@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">


                <div class="panel-heading">
                <form method="post" action="{{ route('account.update',$account->id) }}">
                    <!-- {{ csrf_field() }} -->
                    @csrf
                    @method("PUT")
                    <div class="form-group">
                        <label for="ACC_NO">หมายเลขบัญชี</label>
                        <input type="text" name="ACC_NO" value="{{ $account->ACC_NO }}" class="form-countrol" readonly="readonly">
                    </div>

                    <div class="form-group">
                        <label for="ACC_NAME">ชื่อ</label>
                        <input type="text" name="ACC_NAME" value="{{ $account->ACC_NAME }}" class="form-countrol">
                    </div>

                    <div class="form-group">
                        <label for="ACC_Surname">นามสกุล</label>
                        <input type="text" name="ACC_Surname" value="{{ $account->ACC_Surname }}" class="form-countrol">
                    </div>

                    <div class="form-group">
                        <label for="Balance">จำนวนเงิล</label>
                        <input type="text" name="Balance" value="{{ $account->Balance }}" class="form-countrol">
                    </div>

                    <div>
                        <button type="submit" class="btn btn-default">Edit</button>                    
                    </div>
                
                </form>
                </div>
               
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
