


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 700px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <ul class="nav nav-pills nav-stacked">
        <li><a href="/">หน้าหลัก</a></li>
        <li><a href="/product">สินค้า</a></li>
        <li class="active"><a href="/export">เบิกสินค้า</a></li>
        <li><a href="/borrow">ยืมสินค้า</a></li>
        <li><a href="/employee">พนักงาน</a></li>
      </ul><br>
    </div>
    <div class="col-sm-10 text-left"> 


      <div class="well">
        <h4>ใบเบิกสินค้า</h4>
      </div>

      <div class="well">
        <form method="post" action="{{ route('export.update',$export->id)  }}">
        @csrf
        @method("PUT")
          <div class="form-group">
            <label for="exp_id">เลขที่ใบเบิก</label>
            <input type="text" name="exp_id" class="form-countrol">
          </div>
          <div class="form-group">
            <label for="exp_date">วันที่เบิกสินค้า</label>
            <input type="datetime-local" name="exp_date" class="form-countrol">
          </div>

          <div class="form-group">
            <label for="approve_emp_id">ผู้อนุมัติ</label>
            <input type="text" name="approve_emp_id" class="form-countrol">
          </div>
          <div>
            <button type="submit" class="btn btn-default">แก้ไข</button>                   
          </div>
        </form>
      </div>



    </div>


    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>


</body>
</html>


