<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Export;

class ExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  SELECT * FROM Exports LEFT JOIN Export_details ON Exports.exp_id = Export_details.exp_id


        $export = DB::table('Exports')
            ->leftJoin('Employees', 'Employees.emp_id', '=', 'Exports.approve_emp_id')
            ->get();
กำลังจะ เอา รีเควส

SELECT * FROM Exports e 
JOIN Export_details d ON e.exp_id = d.exp_id 
JOIN Products p ON p.prod_id = d.prod_id 
JOIN Employees m ON d.request_emp_id = m.emp_id
WHERE e.exp_id='x0001'




        return view('export.export',compact('export'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('export.addExport');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       
       
       
        $request->validate(
            [
                'exp_id' => 'required|max:5',
                'approve_emp_id' => 'required|max:5',
                'prod_id' => 'required|max:5',
                'exp_qty' => 'required',
                'request_emp_id' => 'required|max:5',
            ],
            [
                'exp_id.required' => 'กรุณาใส่รหัสใบเบิกสินค้า',
                'exp_id.max' => 'รหัสใบเบิกสินค้าต้องใส่จำนวน 5 ตัวอักษร',
                'approve_emp_id.required' => 'กรุณาใส่รหัสพนักงานที่อนุมัติให้เบิกสินค้า',
                'approve_emp_id.max' => 'รหัสพนักงานที่อนุมัติให้เบิกสินค้า ต้องใส่จำนวน 5 ตัวอักษร',

                'prod_id.required' => 'กรุณาใส่รหัสสินค้า',
                'prod_id.max' => 'รหัสสินค้าใส่ต้องใส่จำนวน 5 ตัวอักษร',

                'exp_qty.required' => 'กรุณาใส่จำนวนของสินค้าที่ต้องการเบิก',
                'request_emp_id.required' => 'กรุณาใส่รหัสพนักงานที่ต้องการเบิกสินค้า',
                'request_emp_id.max' => 'รหัสพนักงานที่ต้องการเบิกสินค้า ต้องใส่จำนวน 5 ตัวอักษร',

            ]


        );
        $export = new Export;
        $export ->exp_id = $request->get('exp_id');
        $export ->approve_emp_id = $request->get('approve_emp_id');
        $export ->exp_date = date('Y-m-d H:i:s');
        $export ->prod_id = $request->get('prod_id');
        $export ->exp_qty = $request->get('exp_qty');
        $export ->request_emp_id = $request->get('request_emp_id');

        $export ->save();
        return redirect('export');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $export = DB::table('Employees')
            ->join('Export_details', 'Employees.emp_id', '=', 'Export_details.request_emp_id')
            ->join('Products', 'Products.prod_id', '=', 'Export_details.prod_id')
            ->where('Export_details.exp_id', '=', $id)
            ->get();

        return view('export.exportDetail',compact('export'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $export = Export::where('exp_id', $id)->first();
        return view('export.editExport',compact('export'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $export = Export::where('exp_id', $id)->first();
        $request->validate(
            [
                'exp_id' => 'required|max:5',
                'approve_emp_id' => 'required|max:5',
                'prod_id' => 'required|max:5',
                'exp_qty' => 'required',
                'request_emp_id' => 'required|max:5',

            ],
            [
                'exp_id.required' => 'กรุณาใส่รหัสใบเบิกสินค้า',
                'exp_id.max' => 'รหัสใบเบิกสินค้าต้องใส่จำนวน 5 ตัวอักษร',
                'approve_emp_id.required' => 'กรุณาใส่รหัสพนักงานที่อนุมัติให้เบิกสินค้า',
                'approve_emp_id.max' => 'รหัสพนักงานที่อนุมัติให้เบิกสินค้า ต้องใส่จำนวน 5 ตัวอักษร',

                'prod_id.required' => 'กรุณาใส่รหัสสินค้า',
                'prod_id.max' => 'รหัสสินค้าใส่ต้องใส่จำนวน 5 ตัวอักษร',

                'exp_qty.required' => 'กรุณาใส่จำนวนของสินค้าที่ต้องการเบิก',
                'request_emp_id.required' => 'กรุณาใส่รหัสพนักงานที่ต้องการเบิกสินค้า',
                'request_emp_id.max' => 'รหัสพนักงานที่ต้องการเบิกสินค้า ต้องใส่จำนวน 5 ตัวอักษร',

            ]
        );
       
        $export->exp_id = $request->exp_id;
        $export->approve_emp_id = $request->approve_emp_id; 
        $export->exp_date = date('Y-m-d H:i:s');
        $export->prod_id = $request->prod_id;
        $export->exp_qty = $request->exp_qty;
        $export->request_emp_id = $request->request_emp_id;


        DB::table('Exports')->where('exp_id', $id)->update([
            'approve_emp_id' => $request->approve_emp_id,
            'exp_date' => $request->exp_date,
            'prod_id' => $request->prod_id,
            'exp_qty' => $request->exp_qty,
            'request_emp_id' => $request->request_emp_id
            ]);
    
        return redirect()->route('export');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $export = Export::where('exp_id', $id);
        $export->delete();
        return redirect("export");
    }
}









