<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('account','AccountController');
Route::resource('product','ProductController');
Route::resource('categories','CategoriesController');
Route::resource('export','ExportController');
Route::resource('borrow','BorrowController');
Route::resource('insertproduct','InsertProductController');
Route::resource('addexport','AddExportController');
Route::resource('addborrow','AddBorrowController');
Route::resource('employee','EmployeeController');
Route::resource('editemployee','EditEmployeeController');
Route::resource('editexport','EditExportController');
Route::resource('editborrow','EditBorrowController');
Route::resource('editproduct','EditProductController');
Route::resource('exportdetail','ExportDetailController');

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/account', function () {
//     return view('account.index');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
